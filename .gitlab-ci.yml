stages:
  - static_tests
  - merge_tests
  - project_setup
  - build
  - dynamic_tests
  - deploy

include:
  - remote: 'https://api.r2devops.io/job/r/r2devops-bot/links_checker/0.2.0.yml?ignore=true.yml'
  - remote: 'https://api.r2devops.io/job/r/r2devops-bot/spell_check/0.2.1.yml?ignore=true.yml'

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

ci_linter:
  stage: static_tests
  image:
    name: alpine/httpie:2.4.0
    entrypoint: [""]
  variables:
    GITLAB_CI_LINT_URL: "https://gitlab.gnome.org/api/v4/ci/lint?include_merged_yaml=true"
    JOB_FILES_EXTENSION: ".yml"
  script:
    - apk add --no-cache jq
    - mkdir ci_linter_reports
    - for JOB_PATH in jobs/*; do
    -   echo "Start checking ${JOB_PATH}/${JOB} using Gitlab CI linter API"
    -   JOB=$(basename ${JOB_PATH})
    -   jq --null-input --arg yaml "$(cat ${JOB_PATH}/${JOB}${JOB_FILES_EXTENSION})" '.content=$yaml' | http --print 'b' --check-status "$GITLAB_CI_LINT_URL" Content-Type:"application/json" > ci_linter_reports/${JOB}.json
    -   if [ $(jq -r '.status' ci_linter_reports/${JOB}.json) == "valid" ]; then
    -     echo "👍 job ${JOB_PATH}/${JOB} is valid"
    -   else
    -     echo "👎 job ${JOB_PATH}/${JOB} is invalid"
    -     jq '.' ci_linter_reports/${JOB}.json
    -     exit 1
    -   fi
    - done
  artifacts:
    expose_as: "job_ci_lint"
    paths:
      - ci_linter_reports/
    when: always

job_structure:
  image: python:3.9.1-alpine
  stage: static_tests
  variables:
    PIPENV_PIPFILE: tools/job_structure/Pipfile
    JOB_LOGFILE: "job_structure.log"
    PYTHONPATH: "./:$PYTHONPATH"
  before_script:
    - pip install --ignore-installed distlib pipenv
    - pipenv install
  script:
    - pipenv run python3 tools/job_structure/job_structure.py
  artifacts:
    expose_as: "job_structure"
    paths:
      - ${JOB_LOGFILE}
    when: always

job_customs:
  image: python:3.9.1-alpine
  stage: static_tests
  variables:
    PIPENV_PIPFILE: tools/job_customs/Pipfile
    JOB_LOGFILE: "job_customs.log"
    PYTHONPATH: "./:$PYTHONPATH"
  before_script:
    - pip install --ignore-installed distlib pipenv
    - pipenv install
  script:
    - pipenv run python3 tools/job_customs/job_customs.py
  artifacts:
    expose_as: "jobs_customs"
    paths:
      - ${JOB_LOGFILE}

job_image_scan:
  image: docker:19.03
  stage: static_tests
  services:
    - name: docker:19.03-dind
      entrypoint: ["env", "-u", "DOCKER_HOST"]
      command: ["dockerd-entrypoint.sh"]
  variables:
    PIPENV_PIPFILE: tools/job_image/Pipfile
    JOB_LOGFILE: "job_image.log"
    OUTPUT_DIR: "scan_output"
    PYTHONPATH: "./:$PYTHONPATH"

    TRIVY_EXIT_ON_SEVERITY: ""
    TRIVY_SEVERITY: "LOW,MEDIUM,HIGH,CRITICAL"
    TRIVY_EXIT_CODE: 0
    TRIVY_VULN_TYPE: "os,library"
    TRIVY_NO_PROGRESS: "false"
    TRIVY_OUTPUT: "junit-report.xml"
    TRIVY_IGNOREFILE: .trivyignore
    TRIVY_CACHE_DIR: .trivycache/
    TRIVY_FORMAT: "template"
    TEMPLATE_NAME: "junit.tpl"
    TRIVY_CLEAR_CACHE: "false"
    TRIVY_IGNORE_UNFIXED: "false"
    TRIVY_DEBUG: "false"

    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    TRIVY_VERSION: "0.9.2"
    TRIVY_REMOTE: ""
    TRIVY_TIMEOUT: ""
    TRIVY_LIGHT: "false"
    TRIVY_DOWNLOAD_DB_ONLY: "false"
    TRIVY_TOKEN: ""
    TRIVY_QUIET: "false"
    TRIVY_SKIP_UPDATE: "false"

  before_script:
    - apk add --no-cache python3 py3-pip
    - pip install --ignore-installed distlib pipenv
    - pipenv install

    - wget https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz
    - tar zxvf trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz
    - wget -O $TEMPLATE_NAME https://github.com/aquasecurity/trivy/raw/v${TRIVY_VERSION}/contrib/junit.tpl

    - mkdir ${OUTPUT_DIR}
  script:
    # We force database download once so we don't do much GitHub Api calls
    - ./trivy --cache-dir ${TRIVY_CACHE_DIR} image --download-db-only
    - for JOB in $(ls -A jobs); do
    -   IMAGE=$(pipenv run python3 tools/job_image/job_image.py ${JOB})
    -   if [ ! -z ${IMAGE} ]; then
    -     NAME=$(basename ${IMAGE})
    -     ./trivy --skip-update --template "@${TEMPLATE_NAME}" --cache-dir ${TRIVY_CACHE_DIR} -o ${OUTPUT_DIR}/${NAME}.${TRIVY_OUTPUT} ${IMAGE}
    -     if [ ! -z ${TRIVY_EXIT_ON_SEVERITY} ]; then
    -       ./trivy --skip-update --template "@${TEMPLATE_NAME}" --cache-dir ${TRIVY_CACHE_DIR} --exit-code 1 --severity ${TRIVY_EXIT_ON_SEVERITY} -o ${OUTPUT_DIR}/${NAME}-failed-${TRIVY_OUTPUT} ${IMAGE}
    -     fi
    -   fi
    -   IMAGE=""
    - done
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_PATH_SLUG == "r2devops-hub"'
  cache:
    paths:
      - "$TRIVY_CACHE_DIR"
  artifacts:
    expose_as: "job_image_scan"
    paths:
      - "${JOB_LOGFILE}"
      - "${OUTPUT_DIR}"
    reports:
      junit:
        - "${OUTPUT_DIR}/*.xml"
    expire_in: 30 days
    when: always

links_checker:
  variables:
    # The following hard excluded link is due that this
    # job will be migrated to a new tool soon (see #314)
    LICHE_EXCLUDE: ^([^http]|https://gitlab.com/r2devops/hub/-/forks/new)|^([^http]|https://r2devops.io/.+)|^([^http]|https://api.r2devops.io/.+)
    FAIL_ON_BROKEN: "true"
    LICHE_DIRECTORY: "docs/"
  allow_failure: true

# See https://docs.gitlab.com/ee/api/labels.html
job_gitlab_labels:
  image: python:3.9.1-alpine
  stage: project_setup
  variables:
    PIPENV_PIPFILE: tools/job_gitlab_labels/Pipfile
    JOB_LOGFILE: "job_gitlab_labels.log"
    PYTHONPATH: "./:$PYTHONPATH"
  before_script:
    - pip install --ignore-installed distlib pipenv
    - pipenv install
  script:
    - pipenv run python3 tools/job_gitlab_labels/job_gitlab_labels.py
  artifacts:
    expose_as: "job_gitlab_labels"
    paths:
      - ${JOB_LOGFILE}
    expire_in: 30 days
    when: always
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_PATH_SLUG == "r2devops-hub"'

spell_check:
  variables:
    PYSPELLING_CONFIG: '.spell_check/.pyspelling.yml'


# See https://docs.gitlab.com/ee/api/releases/
# We can only control the link to the hub, the release is still storing the source code
release:
  stage: deploy
  image:
    name: alpine/httpie:2.2.0
    entrypoint: [""]
  variables:
    PIPENV_PIPFILE: tools/notify/Pipfile
  before_script:
    - apk update && apk add --no-cache bash
    - pip install --ignore-installed distlib pipenv
    - pipenv install

  script: >
    PROJECT_ENCODED=$(/bin/bash -c "$(http --ignore-stdin --body https://gist.githubusercontent.com/cdown/1163649/raw/8a35c36fdd24b373788a7057ed483a5bcd8cd43e/gistfile1.sh) && _encode '$CI_PROJECT_PATH'");

    for JOB in jobs/*; do
      JOB=$(basename ${JOB})
      # Retrieve all versions of the job inside the CHANGELOG file, ## [(0.2.0)] - 2021-04-20 => get only this part ()
      VERSIONS=$(sed -rn 's/^##\s*\[\s*([^\ ]*)\s*\]\s*-\s*[0-9]{4}-[0-9]{2}-[0-9]{2}\s*/\1/p' jobs/${JOB}/CHANGELOG.md)
      for VERSION in ${VERSIONS}; do
        #Retrieve the changes between two versions
        CHANGELOG=$(sed -n "/^##\s*\[\s*${VERSION}\s*\]/,/^##/p" jobs/${JOB}/CHANGELOG.md | sed -e '/^$/d' | head -n -1 | tail -n +2)
        #For initial version, we don't have a previous version, so set to default message
        [ -z "${CHANGELOG}" ] && CHANGELOG=$(sed -n "/^##\s*\[\s*$VERSION\s*\]/,/^[^##]/p" jobs/${JOB}/CHANGELOG.md | sed -e '/^$/d' | tail -n +2)
        STAGE=$(sed -rn 's/^\s*stage\s*:\s*([a-z]*)/\1/p' jobs/${JOB}/${JOB}.yml )
        result=$(http --ignore-stdin POST https://gitlab.com/api/v4/projects/$PROJECT_ENCODED/releases \
          "JOB-TOKEN: ${CI_JOB_TOKEN}" \
          tag_name=${JOB}-${VERSION} \
          ref=${CI_COMMIT_SHA} \
          "description=${CHANGELOG}" \
          'assets:={"links": [{"name": "Hub link", "url": "https://jobs.r2devops.io/'"${VERSION}/${JOB}"'.yml"}]}')

          if [ $(echo $result | grep "Release already exists\|${JOB}-${VERSION}" | wc -l) -eq 0 ]; then
            echo "[ERROR] Problem when attempting to create release ${JOB}-${VERSION}"
            echo "[ERROR] ${result}"
            exit 1;
          else
            if [ $(echo ${result} | grep "Release already exists" | wc -l) -eq 0 ]; then
              echo "New version detected for $JOB-$VERSION, sending notification to discord"
              pipenv run python tools/notify/discord_release_notify.py -n ${JOB} -v ${VERSION} -c "${CHANGELOG}" -s ${STAGE}
            fi

            echo "Processed ${JOB}-${VERSION} : ${result}";
          fi

      done
    done
  rules:
   - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_PATH_SLUG == "r2devops-hub"'

refresh_job_av_database:
  image: python:3.9.1-alpine
  stage: static_tests
  variables:
    PIPENV_PIPFILE: tools/job_av/Pipfile
    PYTHONPATH: "./:${PYTHONPATH}"
  script:
    - apk add --no-cache python3 py3-pip clamav util-linux
    - pip install --ignore-installed distlib pipenv
    - pipenv install

    # Updating the ClamAV database to push it into the cache
    - freshclam

    # Runner cannot access /var/lib/clamav, so we need to copy it
    # in order to cache it properly
    - mkdir freshclam_db
    - cp /var/lib/clamav/* freshclam_db
  cache:
    key: "clamav-db"
    paths:
      - freshclam_db
    policy: push
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CI_CLAMAV_PIPELINE && $CI_PROJECT_PATH_SLUG == "r2devops-hub"'

generate_job_av:
  image: python:3.9.1-alpine
  stage: static_tests
  dependencies:
    - refresh_job_av_database
  variables:
    PIPENV_PIPFILE: tools/job_av/Pipfile
    JOB_LOGFILE: "generate_job.log"
    GENERATED_YAML: "generated-gitlab-ci.yml"
    PYTHONPATH: "./:${PYTHONPATH}"
    SCANNED_IMAGES_FILE: "clamav_scanned_images.json"
  script:
    - if [ $CI_CLAMAV_PIPELINE ]; then echo "[]" > ${SCANNED_IMAGES_FILE}; fi
    - if [ -f "${SCANNED_IMAGES_FILE}" ]; then cat ${SCANNED_IMAGES_FILE}; fi
    - apk add --no-cache python3 py3-pip util-linux
    - pip install --ignore-installed distlib pipenv
    - pipenv install

    - pipenv run python3 tools/job_av/job_av.py
  artifacts:
    paths:
      - ${GENERATED_YAML}
    expire_in: 30 days
    when: always
  cache:
    key: "clamav-scanned-jobs"
    paths:
      - clamav_scanned_images.json
    policy: pull-push
  rules:
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE == "merge_train" && $CI_PROJECT_PATH_SLUG == "r2devops-hub"'
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CI_CLAMAV_PIPELINE && $CI_PROJECT_PATH_SLUG == "r2devops-hub"'

child_job_av:
  stage: merge_tests
  trigger:
    include:
      - artifact: generated-gitlab-ci.yml
        job: generate_job_av
    strategy: depend
  rules:
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE == "merge_train" && $CI_PROJECT_PATH_SLUG == "r2devops-hub"'
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CI_CLAMAV_PIPELINE && $CI_PROJECT_PATH_SLUG == "r2devops-hub"'
