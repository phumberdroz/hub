## Objective

This job will run a predefined `test` command that can be specified in the `package.json` configuration file


## How to use it

1. Make sure that your project has
   [`package.json`](https://yarnpkg.com/configuration/manifest){:target="_blank"}
   file which contains the required `test` command in the `scripts` object
2. Copy the job URL located in the `Install` part of the right panel and add it inside the `include` list of your `.gitlab-ci.yml` file (see the [quick setup](/use-the-hub/#quick-setup)). You can specify [a fixed version](#changelog) instead of `latest`.
3. If you need to customize the job (stage, variables, ...) 👉 check the
   [jobs customization](/use-the-hub/#jobs-customization)
4. Well done, your job is ready to work ! 😀


## Job details

* Job name: `yarn_test`
* Default stage: `tests`
* Docker image: [`node:18-buster`](https://hub.docker.com/_/node){:target="_blank"}
* When: `always`


### Variables

!!! note
    All paths defined in variables are relative and start from the root of your
    repository.

| Name | Description | Default |
| ---- | ----------- | ------- |
| `PROJECT_ROOT` | Path to the directory containing `package.json`  | `.` |
| `YARN_INSTALL_OPTIONS` | Additional options for `yarn install` | ` ` |
| `YARN_TEST_OPTIONS` | Additional options for `yarn test` | ` ` |
| `IMAGE_TAG` | The default tag for the docker image | `18-buster`  |


### Cache

To cache `node_modules` folder for other `npm` jobs, take a look at [`npm_install`](/jobs/others/npm_install/#cache)



### Author
This resource is an **[official job](https://docs.r2devops.io/faq-labels/)** added in [**R2Devops repository**](https://gitlab.com/r2devops/hub) by [@Totara-thib](https://gitlab.com/Totara-thib)
