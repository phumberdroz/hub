# Changelog

All notable changes to this job will be documented in this file.

## [0.1.1] - 2022-07-05
* Fix the addition of environment name in bucket name

## [0.1.0] - 2022-07-05
* Initial version

