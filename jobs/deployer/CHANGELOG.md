# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2022-06-13
* Add docker image tag in variable 

## [0.1.0] - 2021-04-30
* Initial version
